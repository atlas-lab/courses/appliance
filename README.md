# Wellesley GNU/Linux Course Computing Environment

`Vagrantfile` and `csenv.rb` define VirtualBox VM images for two
overlapping GNU/Linux computing environments:

- The **`base` environment** is a headless Fedora 28 system with the
  basic CLI tools for the course. This VM image is used for testing of
  assignment code and sandboxed execution of student code during
  grading.
- The **`desktop` environment** extends the `base` environment with a
  basic GUI desktop (LXDE) and interactive programming tools. This VM
  image is distributed to students to use for coursework on their
  personal computers.

The `desktop` appliance requires at least a minimal update each
semester to install a semester-specific tool even if none of the other
software is updated.

Currently the appliances include dependencies for Wellesley CS 240
only. More courses are planned in future semesters as Ben teaches
them.

The physical Wellesley CS lab workstation environment is currently
managed separately. (For CS 240, see ../lab.)

## Requirements

To build the appliance:

- [VirtualBox >= 5.2](https://virtualbox.org)
- [Vagrant >= 2.1](https://wwwvagrantup.com)
- `convert` from [ImageMagick](https://www.imagemagick.org/script/index.php) or [GraphicsMagick](http://www.graphicsmagick.org/)
- course-specific tools to include (CodeTub for CS 240)

To run the appliance:

- [VirtualBox >= 5.2](https://virtualbox.org)

## Build

Recipes are reasonably well documented in `csenv.rb`.

### Base Appliance

To spin up a `base` appliance: `make` or `vagrant up base`.

### Desktop Appliance

To build a `desktop` appliance OVA for distribution to students (e.g.,
in Fall 2018):

``` shell
make csenv-f18.ova

```

where:

- `<path/to/codetub>` is the path to a local working copy of the
  CodeTub source repo
- `<course-tubclient>` is the path to the course-specialized
  `tubclient` wrapper

This build will generate:

- `<csenv-version-id>.ova`: an OVA file for importing a fresh
  appliance into VirtualBox etc. Distribute this to students.
- `docs/panel.png`: a screenshot of desktop environment in the
  appliance. Include this in documentation.

To provision and boot an appliance without generating the OVA or
screenshot, leave off `.ova`.

## Semester Update

To update for a new semester, prepare the new semester's `tubclient`
tool and rerun the build process.  This will be pretty fast if you do
NOT first `make clean` (which destroys the VM) first), since it can
just drop in the new tool and re-export the OVA.  Cleaning first will
build the VM from scratch again (several minutes).

## Fedora Release Updates

To update to a new Fedora release, try editing the definitions to use
the appropriate `bento/fedora-*` box and see what breaks.

## End-User Guide

Use of the appliance is outlined in student-facing documentation in
[`docs/index.md`](docs/index.md), which also appears in finished form
on the CS 240 website here:
https://cs.wellesley.edu/~cs240/docs/common/appliance/

## License

These build scripts are covered by [LICENSE.txt](LICENSE.txt).

License information for the software that these scripts install into
the appliance is detailed in [`docs/index.md`](docs/index.md), the
student-facing documentation (also on the [CS 240
website](https://cs.wellesley.edu/~cs240/docs/common/appliance/)).

# History

- `wx-f15`: starting in Fall 2015, Ben Wood used
  [virtual appliances](https://cs.wellesley.edu/~bpw/wx) based on
  Fedora 22 in CS 240 (Computer Systems) and CS 251 (Programming
  Languages).  This version was built manually, with a custom system
  for running updates from a command repository later.
- `wx-s16`: For Spring 2016, the manual process was partly replaced by
  an [RPM package
  repository](https://cs.wellesley.edu/~bpw/wx/s16/rpms) with a
  package for each course to require the course dependencies,
  including some custom packages, plus a first-run script to install
  selected courses when students first booted the appliance.  This
  iteration supported CS 240 and CS 301 (Compilers); CS 251 support
  was never completed since Ben was not teaching it.
- In Spring 2016, Lyn Turbak continued using `wx-f15` in CS 251.
- Fall 2016, Lyn switched to the current iteration of the appliance
  and developed a script that students used to install CS 251
  dependencies. This approach remained in use through (at least)
  Spring 2018.
- `wx-f16`, `wx-s17`: Fall 2016 and Spring 2017 saw minor refreshes
  (`dnf upgrade`), but continued to use the Fedora 23 system developed
  for `wx-s16`. Ashley DeFlumere continued use of `wx-s17` in CS 240
  during the 2017-2018 year.
- `csenv-f18`: For Fall 2018, Ben constructed a new `desktop`
  appliance based on Fedora 28 (bento/fedora-28) with a fully
  automated build process using Vagrant. The appliance's slimmer
  `base` sibling contains exactly the same CLI tool (minus the desktop
  environment and editors) for use in testing and grading. Initially,
  this appliance supports CS 240 only.

