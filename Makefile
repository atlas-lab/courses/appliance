NAME ?= csenv
VMUSER ?= u

INSTANCE ?= s19
VERSION =
IMAGE = $(NAME)-$(INSTANCE)$(VERSION)
PUBDIR = cs.wellesley.edu:/net/atlas/wx/$(INSTANCE)/appliance

BASE = base
DESKTOP = desktop

default:
	@echo "Targets:"
	@echo "  $(BASE).vm: start a $(BASE) environment"
	@echo "  $(NAME)-<version>.vm: start a named $(DESKTOP) environment"
	@echo "  $(NAME)-<version>.ova: start and export a named $(DESKTOP) environment as an ova"
	@echo "  $(NAME)-<version>.box: start and export a named $(DESKTOP) environment as a box"

SCREENSHOT = ../docs/appliance/panel.png


#### Base Boxes #####################################

$(BASE).vm:
	vagrant up $(BASE)

Dockerfile: csenv.rb
	ruby -e "require '$(PWD)/csenv.rb'; CSEnv::Base.dockerfile(hostname: '$(NAME)')"

#### Desktop Boxes #####################################

.vagrant-plugins:
# Supports a reload (reboot) provisioning step
	vagrant plugin install vagrant-reload
# Auto installs/updates VirtualBox Guest Additions
	#vagrant plugin install vagrant-vbguest
	touch $@

$(IMAGE).vm: .vagrant-plugins
# Provision the appliance, landing autologined as appliance user
	env VMID=$(IMAGE) VMUSER=$(VMUSER) vagrant up $(DESKTOP)
	touch $@

$(IMAGE).ova: $(IMAGE).vm
	vagrant up $(DESKTOP)
# Screenshot the display for the documentation page
	sleep 10
	VBoxManage controlvm $(IMAGE) screenshotpng $(SCREENSHOT)
	convert $(SCREENSHOT) -crop 100%x10%+0+0 $(SCREENSHOT)
# Shut down the appliance
	vagrant halt $(DESKTOP)
# Export a fresh OVA for distribution
	rm -f $@
	VBoxManage export $(IMAGE) --output $@

$(IMAGE).box: $(IMAGE).vm
	vagrant up $(DESKTOP)
# Install the standard vagrant public key
	vagrant ssh $(DESKTOP) -c "umask 0077 && curl https://raw.githubusercontent.com/hashicorp/vagrant/master/keys/vagrant.pub > ~/.ssh/authorized_keys && sudo shutdown -h now"
# Package as a vagrant box
	rm -f $@
	vagrant package $(DESKTOP) --output $@

publish:
	chmod go+r $(IMAGE).ova
#{ova,box}
#	scp -p $(IMAGE).{ova,box} $(PUBDIR)/
	scp -p $(IMAGE).ova $(PUBDIR)/

clean:
	rm -f .vagrant-plugins $(IMAGE).vm $(IMAGE).box $(IMAGE).ova
	vagrant destroy $(DESKTOP)

.PHONY: default clean codetub-latest
