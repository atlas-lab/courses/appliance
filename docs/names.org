* Appliance Names
** wx
   Origins of the name *wx* are bit fuzzy. It may or may not stand for
   Wellesley Systems, Wellesley Xorg, Wellesley Linux, weather, or
   waterlogged xylophone.

** cs240env
   Either (a) the CS 240 Environment or (b) the feeling of wishing to take CS 240 in a semester when you are not taking it.

** wenv
   Wellesley Environment, Wendy... Wenv.
