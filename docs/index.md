---
layout: page
title:  Course Appliance
version: s19
size: 3.25GB
user: u
basebox: the fedora-28 image produced by the [Bento Project](https://github.com/chef/bento)
---
{% capture ova %}{{site.appliance}}-{{page.version}}.ova{% endcapture %}
{% capture boxurl %}{{site.url}}{{site.baseurl}}/tools/{{site.appliance}}-{{page.version}}.box{% endcapture %}
[ova]: {{site.baseurl}}/tools/{{ova}}
[recipe]: https://gitlab.com/atlas-lab/courses/appliance

The *{{site.appliance}} course appliance* provides a self-contained {{site.course}}
computing environment as a virtual machine that you can run alongside
other applications on most personal computers.  It is isolated from
the rest of your computer and independent from the CS workstations.
The {{site.appliance}} appliance is an alternative for those who wish to work on their
own computers rather working on the [CS GNU/Linux workstations in the
lab]({{site.baseurl}}/tools/#env) or [remotely via
SSH]({{site.baseurl}}/docs/common/ssh/).

**Regardless of what physical or virtual machine you use, working in
the lab is great for community and often yields useful tips from
classmates and course staff.**

# Contents
{: .no_toc}

* toc
{: toc}

# Requirements

Most computers from the last 5 years or so should suffice. You need a computer with:

* a fairly recent 64-bit Intel or AMD x86 processor;
* 4GB or more of RAM (2GB could work, but it would be tight);
* one of [these operating system versions](https://www.virtualbox.org/manual/ch01.html#hostossupport)
* 8GB of available storage space

You are responsible for **backing up any data of importance** stored
on your appliance.  This happens naturally if you employ good
**version control** practice on your work.

# Installation

## Standard Installation

Use these directions to install the course appliance on your
computer. {{site.course}} staff will provide support if you have
issues.

0. Download [`{{ova}}`][ova] [~{{page.size}}] to your computer.  This will take a while.  Do it on the campus network.
1. While you are waiting, download and install the latest version of [VirtualBox 6](https://www.virtualbox.org/). (**Do not** install the "extension pack.")
2. Once `{{ova}}` has finished downloading, start VirtualBox and choose the menu item ***File > Import Appliance...***.
3. Click the folder icon and browse to select `{{ova}}`, then click ***Continue***.
5. Click ***Import*** to accept the default machine parameters.  Importing will take a few minutes.
6. In the VirtualBox manager application,  select the appliance on the left and click the ***Start*** button (green arrow) to start up the appliance.
7. When the appliance has started, you should see its [desktop environment](#use-the-appliance).
8. If text/icons in the appliance window are too tiny: In the VirtualBox manager
   application, select the appliance and click the ***Settings***
   button (orange gear). Select ***Display*** and raise the ***Scaling
   Factor***, perhaps to 200%.
8. Optionally, remove the `{{ova}}` file from your computer to save space.
9. Read about how [VirtualBox captures your mouse and keyboard input
   and how to release them with the *host
   key*](https://www.virtualbox.org/manual/ch01.html#keyb_mouse_normal).

## Advanced Alternatives

If you definitely know what you are doing and you prefer to install or
configure your own {{site.course}} environment **without any support
from {{site.course}} staff** instead of using the [standard
installation](#standard-installation), you may try these alternatives:

{% comment %}
- If you use [Vagrant](https://vagrantup.com), try `vagrant init
  {{site.appliance}}-{{site.instance}} {{boxurl}}`.
{% endcomment %}
- If you wish to reproduce the environment on your own GNU/Linux
  system, the full [appliance recipe][recipe] is available. The file
  `csenv.rb` includes lists of Fedora `PACKAGES` to install, Git
  `REPOS` to clone, and other `DOWNLOADS`.

# Use the Appliance

The appliance automatically logs in as the user `{{page.user}}`.  This
account is local to your appliance. It does not share storage or
preferences with your CS account. Use [version control or
`scp`]({{site.baseurl}}/tools/#general) to transfer files between them.

The appliance presents a fairly minimal interface, focused on a few
relevant programming tools.

<img src="panel.png" alt="Top panel of the appliance desktop environment" title="Top panel of the appliance desktop environment" />

From left to right, the top panel holds:

* **Upper left:** application menu, file browser,
  [terminal]({{site.baseurl}}/tools/#linux),
  [Emacs]({{site.baseurl}}/tools/#emacs)
* **Upper right:** network status, clock, [logout/shutdown](#startup-and-shutdown)

{% comment %}
## Host-Guest Integration

The appliance comes with extensions installed in the guest
operating system to support some useful features such as sharing the
clipboard between the host (the software on your physical computer)
and the guest (the software running inside the appliance).

Check the *Devices* menu in the running *VirtualBox VM* application to
enable or disable integration features.

Read the [VirtualBox
manual](https://www.virtualbox.org/manual/ch04.html) for more
information.
{% endcomment %}

# Startup and Shutdown
   
**Start up** the appliance: open VirtualBox, select the appliance
in the list on the left, and click the *Start* button.  This launches
a separate application running the appliance.

{:#shutdown}
**Shut down** the appliance: click the icon in the **far upper
right corner** of the appliance screen and then click ***Shut down***.

<img src="panel.png" alt="Top panel of the appliance desktop environment" title="Top panel of the appliance desktop environment" />

{% alert Do NOT use *Power Off Machine* or *Reset*! %}

These options are equivalent to yanking out the battery or power cord
while the computer is running or holding down the power button to hard
reset.  These tend to result in corrupted files. It is OK to *Send the
shutdown signal*, which asks the appliance operating system to shut
itself down gracefully.

<img src="vbox-close.png" style="width: 319px;" alt="Do NOT 'power off' the appliance!" title="Do NOT 'power off' the appliance!" />

{% endalert %}

# Pause and Resume

Pausing and resuming the appliance is convenient if you want to keep
some windows open in the appliance but you want to free up resources
on your computer while you work on something else.

**Pause** the appliance: click to close the VirtualBox VM window
that contains the appliance display.  Choose **Save the machine state** and click
OK.

VirtualBox VM will take a few seconds to save the appliance's
current state, then it will close the appliance.  The listing for
the appliance in the VirtualBox manager will now show it as *Saved*
(vs. *Powered Off*).

<img src="vbox-pause.png" style="width: 319px;" alt="Pause the appliance." title="Pause the appliance." />

**Resume** the appliance: select the appliance in the list on the
left and click the *Start* button.

{% comment %}
{:#ram}
# Performance and Memory Usage

By default, the appliance will use 768MB of your computer's RAM.  If you are using memory intensive applications like Eclipse or if you find that the appliance is sluggish, you will need to give the appliance more RAM.

- For CS 240, 768MB (the default, or maybe even 512MB) will probably suffice.
- For CS 301, try 2048-4096MB.  (IntelliJ may be sluggish otherwise.)

If you need to adjust the appliance RAM **at installation time**, scroll down in the ***Appliance settings*** window to find the ***RAM*** entry and double-click to edit it.

![appliance RAM at install](ram.png)

If you need to adjust the appliance RAM **after installation is complete**:

1. [Shutdown](#shutdown) the appliance.
2. In the VirtualBox manager application, click the ***Settings*** button and choose the ***System*** tab.
3. Change the ***Base Memory***:
4. Click ***OK*** to save the change.
5. Start the appliance again.

***If your adjustment causes performance problems on your computer (or the VM itself is still sluggish) please seek assistance from you instructor.***

{% endcomment %}

{% comment %}
# Updates

If your appliance prompts you to install updates, do so.  (You may need to enter `{{page.user}}`'s password.)

From time to time, we may need to install new software or make changes
to your copy of the appliance.  We will let you know when this is
needed.  To get updates, run `sudo dnf update` in a terminal.  This
may prompt for your `{{page.user}}` password.
{% endcomment %}

# Troubleshooting

If the following suggestions do not help, please report issues to your
instructor.  Detailed descriptions and any troubleshooting you have
tried (or solutions you have found) are useful.

### Network Trouble: "Could not resolve hostname"

**Symptoms:** Trouble connecting to network services (*e.g.*, with
version control operations), typically including "could not resolve
hostname."

**Cause:** The appliance has likely lost its network connection.
This can happen when your physical computer temporarily loses and then
regains its wireless network connection (a common occurence at
Wellesley).  Sometimes the appliance does not notice that the
network has returned.

**Confirmation:**

1. Check that your physical computer has network access (can you access the web)?
2. Check that the appliance does not have network access.
   - The appliance screen shows a "two screens" icon in the **upper
     right** when connected: <img src="connected.png" style="width:
     24px; vertical-align: middle;" />
   - The icon includes a red X when disconnected: <img
     src="disconnected.png" style="width: 24px; vertical-align: middle;" />

**Solution:** In the *VirtualBox VM* application (the frame around the
appliance display), **disconnect** then **reconnect** the virtual
network adapter to make the appliance notice the connection.

There are two ways to disconnect/reconnect the network adapter:

- Click the tiny "two screens" icon in the **lower right** status bar of
  the *VirtualBox VM* window  (in the frame around the
  appliance display, **not** the icon you just examined in the 
  upper right).
  
  <img src="vm-network-adapter.png" style="width: 326px;" />
- OR: Choose the *Devices > Network Adapter* menu in the *VirtualBox VM*
  menu bar.

Uncheck, then re-check the *Connect Network Adapter* option.  After a
few seconds, the appliance should reconnect to the network.

### On Windows: "VT-x" or "AMD-V" Unavailable/Disabled or "kernel requires an x86_64 cpu, but only detected an i686 cpu"

**Symptoms:**
When trying to start your appliance from VirtualBox, some Windows
computers may give an error about "VT-x" or "AMD-V" being unavailable
or disabled.

**Cause:**
"VT-x" and "AMD-V" are Intel and AMD's special hardware support for
efficient virtual workstations.  If you have a relatively recent
computer (last few years), it is likely you have these features, but
they may be disabled on some Windows computers.

**Solution:**
You will need to change a low-level setting outside the operating
system to enable hardware support for virtualization.  It requires
changing a low-level setting, but is fairly straightforward.

To enable virtualization support on a computer with UEFI (newer
computers):

1. Hold the `shift` key while pressing restart to reboot your computer
   and access the UEFI settings.  You should see an interface other
   than the usual Windows startup. If you do not, try the BIOS
   directions below.
2. Select *Troubleshoot*, then *Advanced Settings*, then *UEFI Firmware Settings*.
2. Find *Security* or *Virtualization Settings* (or something
   similar).
3. Enable hardware support for virtualization.
4. Select something to the effect of "save and reset" to save the
   settings changes and reboot the computer.

To enable virtualization support on a computer with BIOS:

1. Reboot your computer and hold the F2 or F10 key as it is starting
   up.  This will cause it to enter a mode (known as BIOS or firmware
   settings) allowing you to set low-level hardware options.  (If the
   first of F2/F10 that you try doesn't work, try the other.  The
   screen probably tells you which.)
2. Use the arrow keys to find the *Security* or *Virtualization
   Settings* (or similary named) section, possibly within a
   *Processor* or *CPU* section.
3. Enable hardware support for virtualization.
4. Select something to the effect "save and reset" to save the
   settings changes and reboot the computer.

More reference [here](http://www.howtogeek.com/213795/how-to-enable-intel-vt-x-in-your-computers-bios-or-uefi-firmware/) and [here](http://www.howtogeek.com/175649/what-you-need-to-know-about-using-uefi-instead-of-the-bios/).

You may need to delete the virtual machine you created in the
VirtualBox manager and reimport `{{ova}}` for it to
recognize the virtualization support.

{:#implementation}
# Software and Licenses

The course appliance is built from
[free](https://www.fsf.org/about/what-is-free-software) and
[open-source](https://opensource.org/osd) software.

The appliance is based on {{page.basebox}} and contains installations
of:

- the [Fedora](https://getfedora.org) distribution of the GNU/Linux
  operating system
- the [LXDE ](https://lxde.org/) desktop environment
- the [VirtualBox](https://www.virtualbox.org) Guest Additions
- [CodeTub](https://gitlab.com/atlas-lab/courses/codetub)
- [several programming tools]({{site.baseurl}}/tools/)

The contents of the appliance image are distributed under the
licenses for the Fedora Project and the individual component packages.
Licenses and source for the Fedora Project are available
[here](https://fedoraproject.org/wiki/Legal:Licenses/LicenseAgreement?rd=Legal/Licenses/LicenseAgreement).

The appliance is packaged and run using:

- the [VirtualBox](https://www.virtualbox.org) virtualization platform
([documentation](https://www.virtualbox.org/manual/UserManual.html))
- the [Vagrant](https://vagrantup) virtual machine manager

The [definition used to prepare the appliance image][recipe] is also
available.
