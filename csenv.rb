module CSEnv
  
  module Shell
    def self.run(config, *commands, always: false)
      config.vm.provision(:shell, inline: commands.join(" && "), run:
                                                                   if always
                                                                     "always"
                                                                   else
                                                                     "once"
                                                                   end)
    end
    def self.rm(config, *paths, always: false)
      run(config, "rm -rf " + paths.join(" "), always: always)
    end
    def self.mv(config, source, dest, always: false)
      run(config, "mv " + source + " " + dest, always: always)
    end
    def self.mkdirp(config, *paths, always: false)
      run(config, "mkdir -p " + paths.join(" "), always: always)
    end
    UPLOAD_DIR = "/tmp/__upload"
    def self.upload(config, source, dest, owner: nil, mode: nil, always: false)
      mkdirp(config, UPLOAD_DIR, always: always)
      run(config, "chmod 1777 #{UPLOAD_DIR}", always: always)
      tmp = File.join(UPLOAD_DIR, File.basename(dest))
      rm(config, tmp, always: always)
      config.vm.provision(:file, source: source, destination: tmp, run:
                                                                     if always
                                                                       "always"
                                                                     else
                                                                       "once"
                                                                     end)
      rm(config, dest, always: always)
      mv(config, tmp, dest, always: always)
      if owner
        run(config, "chown -R " + owner + " " + dest, always: always)
      end
      if mode
        run(config, "chmod -R " + mode + " " + dest, always: always)
      end
    end
    def self.reboot(config)
      config.vm.provision(:reload)
    end
  end

  module DNF

    INSTALL_ARGS = "-y -q --setopt=install_weak_deps=False"
    
    def self.dnfinstall(config, pkgs)
      Shell.run(config,
                "dnf install #{INSTALL_ARGS} " + pkgs.join(" "))
    end

    def self.dnfgroupinstall(config, groups)
      Shell.run(config,
                "dnf group install -y -q --setopt=install_weak_deps=False " + groups.map {|g| "'#{g}'"}.join(" "))
    end

    def self.dnfupgrade(config, pkgs = [])
      Shell.run(config,
                "dnf upgrade -y -q --setopt=install_weak_deps=False " + pkgs.join(" "))
    end

    def self.dnfremove(config, pkgs)
      Shell.run(config,
                "dnf remove -y -q " + pkgs.join(" "))
    end

    def self.dnfautoremove(config, pkgs = [])
      Shell.run(config,
                "dnf autoremove -y -q " + pkgs.join(" "))
    end

    def self.dnfclean(config, targets = ["all"])
      Shell.run(config,
                "dnf clean " + targets.join(" "))
    end
  end

  module Base

    DISTRO_NAME = "fedora"
    DISTRO_VERSION = "28"
    DEFAULT_HOSTNAME = "appliance"

    SET_TIMEZONE = [
      "rm -f /etc/localtime",
      "ln -s /usr/share/zoneinfo/America/New_York /etc/localtime",
    ]
    
    # CLI tools for all course environments.
    PACKAGES = [
      # Version control and sync
      "git", "rsync",

      #### CS 240 tools
      
      # Language implementations
      "gcc", "glibc", "glibc-devel", "glibc-headers", "glibc-static", "libgcc", "readline-devel",
      "java-1.8.0-openjdk-headless", "java-1.8.0-openjdk-devel",
      "perl",
      "python3",

      # Build tools
      "make", "ant",

      # Debugging and testing tools
      "gdb", "valgrind",

      # CS 301 tools
      "scala", "sbt",
    ]

    # Install base course environment.
    def self.provision(config, hostname: DEFAULT_HOSTNAME)
      config.vm.box = "bento/#{DISTRO_NAME}-#{DISTRO_VERSION}"

      config.vm.synced_folder(".", "/vagrant", disabled: true)
      
      config.vm.provider(:virtualbox) do |vb|
        # Resources: 768MB RAM, 256MB swap, 1 cpu.
        vb.memory = "768"
        vb.cpus = 1
        # Ensure remote display is off
        vb.customize(["modifyvm", :id, "--vrde", "off"])
      end

      # Set timezone.
      Shell.run(config, SET_TIMEZONE)

      # Name host for prompt niceties
      Shell.run(config, "hostname " + hostname)

      # Upgrade.
      DNF.dnfupgrade(config)

      # Install.
      DNF.dnfinstall(config, PACKAGES)

      # Upload things.
      Shell.upload(config, "./bin/gcc-nopie", "/usr/local/bin/gcc",
                   owner: "root:root", mode: "u=rwx,go=rx")
      
      # Shrink swap.
      Shell.run(config,
                "swapoff /swapfile",
                "rm -f /swapfile",
                "dd if=/dev/zero of=/swapfile bs=1M count=256",
                "chmod 600 /swapfile",
                "mkswap /swapfile",
                "swapon /swapfile")
    end

    # Generate a Dockerfile for this environment.
    def self.dockerfile(hostname: DEFAULT_HOSTNAME)
      include DNF
      File.open("./Dockerfile", "w") do |df|
        df.puts(
          <<-END
# Auto-generated by script.
FROM fedora:28
RUN #{SET_TIMEZONE.join(' && ')}
RUN dnf upgrade #{INSTALL_ARGS}
RUN dnf install #{INSTALL_ARGS} #{PACKAGES.join(' ')}
ADD --chown=root:root ./bin/gcc-nopie /usr/local/bin/gcc
WORKDIR /sandbox
CMD cat > /dev/null
END
        )
      end
    end

  end

  module Desktop

    PACKAGES = [
      "emacs",
    ]

    REMOVE_PACKAGES = [
      "dnfdragora",
      "xscreensaver-*",
      "gigolo",
      "opensc",
      "clipit",
      "setroubleshoot*",
      "gnome-disk-utility",
      "gnome-abrt",
      "gpicview",
      "galculator",
      "system-config-*",
      "firewall*",
      "lxlauncher",
      "spice-vdagent",
      "*vmware",
      "lxsession-edit",
      "xarchiver",
      "xpad",
      "obconf",
      "glibc-all-langpacks",
      "vlgothic-fonts",
      "thai-*",
      "stix-fonts",
      "tabish-*-fonts",
      "sml-*fonts",
      "sil-*-fonts",
      "paktype-*-fonts",
      "naver-*fonts*",
      "lohit-*-fonts",
      "khmeros-*fonts*",
      "julietaula-*-fonts",
      "jomolhari-fonts",
      "adobe-*-fonts",
      "aajohna-*fonts",
      "abattis-*fonts",
      "google-noto-*-fonts",
      "*zhuyin*",
      "*pinyin*",
      "*kkc*",
      "*hangul*",
      "*cangjie*",
      "*ucd*",
      "cups",
      "foomatic"
    ]

    REPOS = [
      {
        # CodeTub
        :src => "https://gitlab.com/atlas-lab/courses/codetub",
        :dest => "/net/atlas/codetub",
      },
    ]

    IDEA_VERSION= "2018.3.4"
    DOWNLOADS = [
      {
        # CS 240 course-specific tubclient
        :src => "https://cs.wellesley.edu/~cs240/tools/cs240tubclient",
        :dest => "/usr/local/bin/cs240",
        :mode => "u=rwx,go=rx",
      },
    ]

    def self.provision(config, username: ENV["VMUSER"] || "u", &custom)
      
      #### VM GUI Parameters ################################

      config.vm.provider(:virtualbox) do |vb|
        # Display the VirtualBox GUI when booting the machine
        vb.gui = true
        vb.name = ENV["VMID"]

        # Ensure remote display is off
        vb.customize(["modifyvm", :id, "--vrde", "off"])
        # Minimum 9MB for fullscreen mode
        vb.customize(["modifyvm", :id, "--vram", "12"])
        # Share clipboard between host and guest
        vb.customize(["modifyvm", :id, "--clipboard", "bidirectional"])
      end

      #### Interactive Software Packages ################################

      # Install LXDE
      DNF.dnfgroupinstall(config, ["LXDE Desktop"])
      
      # Install standard tools (after desktop environment, in case
      # they pull in a bigger default as a dependency)
      DNF.dnfinstall(config, PACKAGES)

      #### Remove extra packages and files ################################

      # After installs, to be sure we can remove parts of installed packages
      
      # Remove a hodgepodge of extraneous packages
      DNF.dnfremove(config, REMOVE_PACKAGES)

      # Clean dnf caches.
      DNF.dnfautoremove(config)
      DNF.dnfclean(config)

      # Remove extra locales (space hog)
      Shell.run(
        config,
        <<-SHELL
for loc in /usr/share/locale/*
do
    case $loc in
         */en);;
         */en_US);;
            *) sudo rm -r $loc;;
    esac
done
SHELL
      )

      # Clone repos.
      REPOS.each do |spec|
        Shell.run(config,
                  "mkdir -p #{File.dirname(spec[:dest])}",
                  "git clone #{spec[:src]} #{spec[:dest]}")
      end

      # Download things.
      DOWNLOADS.each do |spec|
        Shell.run(config,
                  "mkdir -p #{File.dirname(spec[:dest])}",
                  "curl -s -S --retry 2 -L #{spec[:src]} > #{spec[:dest]}",
                  "chmod #{spec[:mode]} #{spec[:dest]}")
      end

      #### Configure Desktop Environment ################################

      # Enable LXDE as default target at next boot
      Shell.run(config,
                "rm -f /etc/systemd/system/default.target",
                "ln -s /etc/systemd/system/graphical.target /etc/systemd/system/default.target",
                "systemctl enable lxdm")

      # Set autologin for username.
      Shell.run(config,
                "echo >> /etc/lxdm/lxdm.conf",
                "echo '[base]' >> /etc/lxdm/lxdm.conf",
                "echo 'autologin=#{username}' >> /etc/lxdm/lxdm.conf")

      # Remove .desktop applications to clean up LXDE interface.
      # After installs to ensure we can remove parts of installed packages
      Shell.rm(config,
               "/etc/xdg/autostart/geoclue*",
               "/usr/share/applications/{lxsession-default-apps.desktop,lxde-desktop-preferences.desktop,java*.desktop,geoclue*.desktop,pcmanfm-desktop-pref.desktop,im-chooser.desktop,nm-connection-editor.desktop,fedora-release-notes*.desktop,libfm-pref-apps.desktop,lxrandr.desktop}")
      
      # Customize default LXDE user config
      Shell.upload(config, "./skel/.config.tar.gz", "/tmp/skel.config.tar.gz",
                   owner: "root:root")
      Shell.run(config, "cd /etc/skel && tar xfz /tmp/skel.config.tar.gz",
                "chown -R root:root /etc/skel",
                "rm /tmp/skel.config.tar.gz")

      # Put codetub tools on the path.
      Shell.run(config,
                "echo >> /etc/skel/.bash_profile",
                "echo 'export PATH=/net/atlas/codetub/bin:$PATH' >> /etc/skel/.bash_profile")

      # Set EDITOR.
      Shell.run(config,
                "echo >> /etc/skel/.bash_profile",
                "echo 'export EDITOR=emacs' >> /etc/skel/.bash_profile",
                "echo 'export VISUAL=emacs' >> /etc/skel/.bash_profile")

      # # CS 301 IntelliJ IDEA
      # Shell.upload(config,
      #              "./ideaIC-#{IDEA_VERSION}.tar.gz",
      #              "/usr/local/ideaIC.tar.gz",
      #              owner: "root:root")
      # Shell.run(config,
      #           "cd /usr/local/ && tar xfz /usr/local/ideaIC.tar.gz",
      #           "ln -s /usr/local/idea*/bin/idea.sh /usr/local/bin/idea.sh",
      #           "rm /usr/local/ideaIC.tar.gz")

      # # CS 301 IntelliJ IDEA Scala Plugin
      # Shell.upload(config,
      #              "./scala-intellij-bin-2018.3.6.zip",
      #              "/usr/local/idea-scala-plugin.zip",
      #              owner: "root:root")
      # Shell.run(config,
      #           "cd /usr/local/idea*/plugins && unzip /usr/local/idea-scala-plugin.zip",
      #           "rm /usr/local/idea-scala-plugin.zip")

      if custom
        # Can modify /etc/skel, but not user
        custom.call(config)
      end

      #### Create User, Login, Finish ################################

      # Create end user
      Shell.run(config,
                "useradd --create-home --home-dir /home/#{username} --shell /bin/bash --comment 'End User' #{username}",
                "echo #{username} | passwd --stdin #{username}")

      # Reload with end user LXDE autologin
      Shell.reboot(config)

      # Cleanup home directories.
      Shell.run(config,
                "rm -rf /home/*/{Desktop,Documents,Downloads,Music,Pictures,Public,Templates,Videos}")

      # CS 251
      ["racket", "sml"].each do |pkg|
        Shell.upload(config, "./#{pkg}s19-installer.tar.gz", "/usr/local/#{pkg}s19-installer.tar.gz")
        # Shell.run(config, "su -l u -c 'tar xpfz /usr/local/#{pkg}s19-installer.tar.gz'",
        #           "rm /usr/local/#{pkg}s19-installer.tar.gz",
        #           "su -l u -c '#{pkg}s19-installer/install.sh'",
        #           "su -l u -c 'rm -rf #{pkg}s19-installer/'")
      end

    end
  end

end

module SandboxEnv
  def self.configure(config)
    CSEnv::Base.provision(config, hostname: "sandboxvm")
  end
end
